/**
 * Based on code from Joe Conway <mail@joeconway.com>
 * https://www.postgresql-archive.org/How-to-run-in-parallel-in-Postgres-td6114510.html
 *
 * Execute a set off stmts in parallel
 *
 */


DROP PROCEDURE IF EXISTS start_execute_parallel(
INOUT _stmts text[],
INOUT connstr text,
INOUT conn_open text[],
INOUT conn_stats_runnnig text[],
INOUT stats_done text[],
INOUT all_stmts_done boolean,
_default_seconds_to_wait int,
_num_parallel_thread int,
_close_open_conn boolean,
_contiune_after_stat_exception boolean);

/**

Execute statements and return after given time and waith for all statements to be done

*/

CREATE OR REPLACE PROCEDURE start_execute_parallel(
INOUT _stmts text[], -- The list of statements to run, when a statemen is started/done its removed from the array
INOUT connstr text, -- This may be null at first call, if null the connect string build up based different env. settings
INOUT conn_open text[], -- This must be null at first call and can not be updated by the caller. This is list of current connections used when function return
INOUT conn_stats_runnnig text[], -- This must be null at first call and can not be updated by the caller. This the list off current running statements, if only NULL values in the array no statements are running
INOUT stats_done text[], -- This should be null when called , and new statements done will be added to this list.
INOUT all_stmts_done boolean, -- Will be true when all statements area done
_default_seconds_to_wait int DEFAULT 1,
_num_parallel_thread int DEFAULT 3, -- number threads/connections to use
_close_open_conn boolean DEFAULT false, -- always close connection before and open before sending next statement
_contiune_after_stat_exception boolean DEFAULT true -- If true, will contiunue to run even if one the statements fails
)
LANGUAGE plpgsql
AS $$
DECLARE
	i int = 1;
	new_stmt text;
	old_stmt text;
	num_stmts_in int;
	num_stmts_executed int = 0;
	num_stmts_running int = 0;
	num_stmts_failed int = 0;
	num_conn_opened int = 0;
	num_conn_free_after_check int;
	num_conn_notify int = 0;
	retv text;
	retvnull text;
	conn_status int;
	rv int;
	new_stmts_started boolean;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;

	--db_name
	db_host text;
	db_name text;
	db_port text;
	db_user text;

	raise_error text;
	raise_error_temp text;

	start_time timestamp  = Clock_timestamp();
	done_time timestamp;
	used_time float;
	connect_key_name text;

	use_execute_parallel_connstring boolean = false;
	execute_parallel_rerun_job text;
	tmp_conn_open text;

begin


	IF connstr IS NULL THEN
		SELECT setting INTO db_port FROM pg_catalog.pg_settings WHERE name = 'port';
		db_name := current_database();
		db_user := current_user;
		SELECT host(inet_server_addr()) INTO db_host;

		IF db_host IS NULL THEN
			RAISE NOTICE 'db_host IS NULL % ', db_host;
			db_host = 'localhost';
		END IF;

		IF coalesce(Array_length(_stmts, 1),0) = 0 THEN
				RAISE NOTICE 'In start_execute_parallel, No statements to execute';
			ELSE
				RAISE NOTICE 'In start_execute_parallel, % statements to execute in % threads', coalesce(Array_length(_stmts, 1),0), _num_parallel_thread;
		END IF;

		-- Check if num parallel theads if bugger than num _stmts
		IF (_num_parallel_thread > coalesce(Array_length(_stmts, 1),0)) THEN
					_num_parallel_thread = coalesce(Array_length(_stmts, 1),0);
		END IF;

		connstr := current_setting('execute_parallel.connstring',true);

		-- Tets on  connstr <> ''  just make create test case more easy since we can unset execute_parallel.connstring in sessin
		IF connstr IS NOT NULL AND connstr <> '' THEN
			use_execute_parallel_connstring := true;
			connstr := current_setting('execute_parallel.connstring');
		ELSIF current_setting('execute_parallel.password',true) IS NOT NULL THEN
			use_execute_parallel_connstring := true;
			connstr := Format('host=%s dbname=%s port=%s user=%s password=%s',
			db_host,db_name,db_port,current_user,current_setting('execute_parallel.password',true));
		ELSE
			-- typical postgres user
			connstr := Format('dbname=%s port=%s',db_name,db_port);
		END IF;
	ELSE
		use_execute_parallel_connstring := true;
	END IF;

	IF coalesce(Array_length(conn_open, 1),0) = 0 THEN
		-- Open connections for _num_parallel_thread
		BEGIN

			connect_key_name = gen_random_uuid();

			for i in 1.._num_parallel_thread loop
				tmp_conn_open := 'conn_'||connect_key_name||'_'||i::text;
				perform dblink_connect(tmp_conn_open, connstr);
				conn_open[i] := tmp_conn_open;
				-- init to null for running state
				conn_stats_runnnig[i] := null;
			end loop;

			num_conn_opened = array_length(conn_open,1);
		EXCEPTION WHEN OTHERS THEN
			num_conn_opened = array_length(conn_open,1);

			GET STACKED DIAGNOSTICS
				v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT,
				v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
				v_context = PG_EXCEPTION_CONTEXT;

			-- Give a hint about execute_parallel.connstring
			v_hint := Format('Have you set execute_parallel.connstring like "set execute_parallel.connstring = %1$L"',
			'host=localhost dbname=nibio_reg user=<user> password=<password>');

			IF num_conn_opened = 0 THEN
				RAISE EXCEPTION 'In start_execute_parallel, Failed to open any connection: % - detail: % - hint: % - context: %',
					v_msg, v_detail, v_hint, v_context;
			END IF;

			RAISE WARNING 'Failed to open all requested connections % , reduce to  % state  : %  message: % detail : % hint % for _stmts %',
			_num_parallel_thread, num_conn_opened, v_state, v_msg, v_detail, v_hint, _stmts;

			-- Check if num parallel theads if bugger than num _stmts
			IF (num_conn_opened < _num_parallel_thread) THEN
				_num_parallel_thread = num_conn_opened;
			END IF;
		END; -- EXCEPTION WHEN OTHERS THEN

		RAISE NOTICE 'In start_execute_parallel, Init new connstr for % statements to execute in % threads', coalesce(Array_length(_stmts, 1),0), _num_parallel_thread;
	ELSE
		num_conn_opened = array_length(conn_open,1);
		-- RAISE NOTICE 'In start_execute_parallel, Use old connstr for % statements to execute left in % threads using %', coalesce(Array_length(_stmts, 1),0), _num_parallel_thread, connstr;
	END IF;


	num_stmts_in = coalesce(Array_length(_stmts, 1),0);

	-- Set stats_done empty
	stats_done = '{}';

	IF (num_conn_opened > 0) THEN
		-- Enter main loop
		LOOP
			all_stmts_done = FALSE;
			num_stmts_running = 0;
			new_stmts_started = false;
			num_conn_free_after_check = 0;
			num_stmts_executed = 0;

			-- check if connections are not used
			raise_error_temp = null;
			FOR i IN 1.._num_parallel_thread loop
				IF (conn_stats_runnnig[i] IS NOT NULL) THEN
					num_stmts_running = num_stmts_running + 1;
					--select count(*) from dblink_get_notify(conn_open[i]) into num_conn_notify;
					--IF (num_conn_notify is not null and num_conn_notify > 0) THEN
					SELECT dblink_is_busy(conn_open[i]) into conn_status;
--          RAISE NOTICE 'In start_execute_parallel, conn_status % for start_execute_parallel, test if done conn_stats_runnnig[i] %', conn_status, conn_stats_runnnig[i];

					IF (conn_status = 0) THEN
						-- try get return value for current statement
						old_stmt := conn_stats_runnnig[i];
--						RAISE NOTICE 'In start_execute_parallel, test if done for stmt (%) on connection %',
--						(CASE WHEN LENGTH(old_stmt) < 100 THEN old_stmt ELSE substring(old_stmt,0,99) END), conn_open[i];
						BEGIN
							LOOP
								select val into retv from dblink_get_result(conn_open[i]) as d(val text);
								EXIT WHEN retv is null;
							END LOOP ;
							-- check if job should rerun
							rv := dblink_send_query(conn_open[i],'show execute_parallel.rerun_job;');
							select val into execute_parallel_rerun_job from dblink_get_result(conn_open[i]) as d(val text);
							LOOP
								select val into retv from dblink_get_result(conn_open[i]) as d(val text);
								EXIT WHEN retv IS NULL;
							END LOOP ;
							--to could not send query: another command is already in progress
							--https://www.postgresql.org/docs/current/contrib-dblink-get-result.html
							--This function must be called if dblink_send_query returned 1. It must be called once for each query sent, and one additional time to obtain an empty set result, before the connection can be used again.
							select val into retv from dblink_get_result(conn_open[i]) as d(val text);

							IF execute_parallel_rerun_job IS NOT NULL AND execute_parallel_rerun_job::boolean = TRUE THEN
								RAISE NOTICE 'In start_execute_parallel, Rerun %', old_stmt;

								-- TODO find out if we should test check _close_open_conn=true and do disconnect and connect again.

								-- rerun job if rerun is returned
								rv := dblink_send_query(conn_open[i],'set execute_parallel.rerun_job = false;');
								LOOP
										select val into retv from dblink_get_result(conn_open[i]) as d(val text);
										EXIT WHEN retv is null;
								END LOOP ;
								-- Start to run the real request
								rv := dblink_send_query(conn_open[i],old_stmt);
							ELSE
								-- job is done
								conn_stats_runnnig[i] := null;
								num_stmts_executed := num_stmts_executed + 1;
								num_stmts_running = num_stmts_running - 1;
								stats_done = array_append(stats_done,old_stmt);
	--							RAISE NOTICE 'In start_execute_parallel, Done with stmt (%) on connection % and retv % ',
	--							(CASE WHEN LENGTH(old_stmt) < 100 THEN new_stmt ELSE substring(old_stmt,0,99) END),
	--							conn_open[i], retv;
							END IF;

						EXCEPTION WHEN OTHERS THEN
							GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
											v_context = PG_EXCEPTION_CONTEXT;
							raise_error_temp = Format('stmt: %L , using conn %L, state  : %L message: %L detail : %L hint : %L context: %L',
							old_stmt, conn_open[i], v_state, v_msg, v_detail, v_hint, v_context);

							num_stmts_failed := num_stmts_failed + 1;
							conn_stats_runnnig[i] := null;

							num_stmts_running = num_stmts_running - 1;
							perform dblink_disconnect(conn_open[i]);
							perform dblink_connect(conn_open[i], connstr);

							IF raise_error IS NULL THEN
								raise_error = raise_error_temp;
							ELSE
								raise_error = raise_error||';;;;;'||raise_error_temp;
							END IF;

							RAISE WARNING 'In start_execute_parallel, Failed get value %', raise_error_temp;
						END; -- END BEGIN/EXCEPTION WHEN OTHERS THEN
					END IF; -- Done try get return value for current statement
				END IF; -- Done checking this connection

				IF conn_stats_runnnig[i] is null AND coalesce(Array_length(_stmts, 1),0) > 0 THEN
					-- try to start next job, get the first statement in list at return and remove that from the job list

					new_stmt := _stmts[1];
					conn_stats_runnnig[i] :=  new_stmt;
					-- Handle null value in statement list
					IF new_stmt is not NULL THEN
						-- start a new jobb new_stmt
						num_conn_free_after_check = num_conn_free_after_check - 1;

						IF _close_open_conn=true THEN
								perform dblink_disconnect(conn_open[i]);
								perform dblink_connect(conn_open[i], connstr);
						END IF;
						--rv := dblink_send_query(conn_open[i],'BEGIN; '||new_stmt|| '; COMMIT;');

						-- if use_execute_parallel_connstring is used, this has to be done before
						IF use_execute_parallel_connstring THEN
							rv := dblink_send_query(conn_open[i],'set execute_parallel.connstring = '||quote_literal(connstr)||'; ');
							LOOP
									select val into retv from dblink_get_result(conn_open[i]) as d(val text);
									EXIT WHEN retv is null;
							END LOOP ;
						END IF;

						rv := dblink_send_query(conn_open[i],'set execute_parallel.rerun_job = false;');
						LOOP
							select val into retv from dblink_get_result(conn_open[i]) as d(val text);
							EXIT WHEN retv is null;
						END LOOP ;

						-- Start to run the real request in try catch
						BEGIN
							rv := dblink_send_query(conn_open[i],new_stmt);
							IF rv = 1 THEN
								new_stmts_started = true;
								-- remove first statement from list
								_stmts := _stmts[2:];
							ELSE
								RAISE NOTICE 'In start_execute_parallel, Failed to send stmt, NOT OK return value from dblink_send_query % for new stmt (%) on connection % using index %',
								rv, new_stmt, conn_open[i],i;
							END IF;

							EXCEPTION WHEN OTHERS THEN
								GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
								-- Give a hint about execute_parallel.connstring
								v_hint := Format('Have you set execute_parallel.connstring like "set execute_parallel.connstring = %1$L"',
								'host=localhost dbname=nibio_reg user=<user> password=<password>');
								RAISE WARNING 'In start_execute_parallel, Failed to send stmt: %s , using conn %, state  : % message: % detail : % hint : % ',
								conn_stats_runnnig[i], conn_open[i], v_state, v_msg, v_detail, v_hint;

								num_stmts_failed := num_stmts_failed + 1;
								perform dblink_disconnect(conn_open[i]);
								perform dblink_connect(conn_open[i], connstr);
						END; -- BEGIN/END EXCEPTION WHEN OTHERS THEN
					END IF;
				END IF; --  try to start next job, get the first statement in list at return and remove that from the job list

			END LOOP; -- try to start next job, get the first statement in list at return and remove that from the job list

			done_time := Clock_timestamp();
			used_time := (Extract(EPOCH FROM (done_time - start_time)));

			IF new_stmts_started = FALSE AND num_stmts_running = 0 AND coalesce(Array_length(_stmts, 1),0) = 0 THEN
				all_stmts_done = TRUE;
			END IF;

			EXIT WHEN all_stmts_done OR used_time > _default_seconds_to_wait OR
			(_contiune_after_stat_exception = false AND raise_error_temp is not null);

			-- Do a slepp if nothings happens to reduce CPU load
			IF (new_stmts_started = false) THEN
				--RAISE NOTICE 'In start_execute_parallel, Do sleep at num_stmts_executed %s num stmts not started array_length= %, new_stmts_started = %',
				--num_stmts_executed, coalesce(Array_length(_stmts, 1),0), new_stmts_started;
				perform pg_sleep(0.0001);
			END IF;
		END LOOP ;

		-- close connections for _num_parallel_thread
		IF all_stmts_done THEN
			for i in 1.._num_parallel_thread loop
				perform dblink_disconnect(conn_open[i]);
			END loop;
		END IF;
	END IF;

--	RAISE NOTICE 'In start_execute_parallel num statements left % to start execute in % threads , num_stmts_running %, num_stmts_failed in this run %',
--	coalesce(Array_length(_stmts, 1),0), _num_parallel_thread, num_stmts_running, num_stmts_failed;


	IF all_stmts_done THEN
		IF num_stmts_failed = 0 AND  coalesce(Array_length(_stmts, 1),0) = 0 THEN
			RAISE NOTICE 'In start_execute_parallel, Done execute % stmts, in % threads, num_stmts_failed in this run %',
			Array_length(stats_done, 1), _num_parallel_thread, num_stmts_failed;
		ELSE
			IF raise_error is not null THEN
				RAISE EXCEPTION 'Num not started statsments  % raise_error %', coalesce(Array_length(_stmts, 1),0), raise_error USING HINT = 'An error happend in one the statemnet, please chem them ';
			END IF;
		END IF;
	END IF;

	--RAISE NOTICE 'In start_execute_parallel, leave *************@ all_stmts_done % in used_time % _default_seconds_to_wait %', all_stmts_done, used_time, _default_seconds_to_wait;

END;
$$
;

GRANT EXECUTE on PROCEDURE start_execute_parallel(
	_stmts text[],
	connstr text,
	conn_open text[],
	conn_stats_runnnig text[],
	stats_done text[],
	all_stmts_done boolean,
	_default_seconds_to_wait int,
	_num_parallel_thread int,
	_close_open_conn boolean,
	_contiune_after_stat_exception boolean
) TO public;



/**

Execute statements and return first when all statements are done.

So this code will not return to caller all jobs are done.

*/

DROP PROCEDURE IF EXISTS do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
);


CREATE OR REPLACE PROCEDURE do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text DEFAULT NULL, -- this will be buildt up inside start_execute_parallel at first call IF NULL
	_num_parallel_thread int DEFAULT 3, -- number threads/connections to use
	_close_open_conn boolean DEFAULT true, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean DEFAULT false -- If true, will contiunue to run even if one the statements fails
)
LANGUAGE plpgsql
AS $$
DECLARE

	connstr text; -- this will be buildt up inside start_execute_parallel at first call IF NULL
	conn_open text[]; -- this will be buildt up inside start_execute_parallel at first call
	conn_stats_runnnig text[]; -- this will be buildt up inside start_execute_parallel at first call
	stats_done text[]; -- This should be null when called , and new statements done will be added to this list.
	all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
	user_connstr text; -- check https://www.postgresql.org/docs/11/contrib-dblink-connect.html
	start_execute_parallel_counter int = 0;
	seconds_to_wait_before_check_result int = 300;

	stmts text[];
	used_time float;
	start_time_delta_job timestamp WITH time zone;
	max_parallel_jobs int;
	contiune_after_stat_exception boolean;
	close_open_conn boolean;
	start_time timestamp WITH time zone;
	loop_number int = 1;


BEGIN

	-- Set input variables
	IF _connstr IS NOT NULL THEN
		connstr = _connstr;
	END IF;

	stmts = _stmts;
	max_parallel_jobs = _num_parallel_thread;
	contiune_after_stat_exception = _contiune_after_stat_exception;
	close_open_conn = _close_open_conn;

	-- Fixed values
	all_stmts_done = false;
	start_execute_parallel_counter = 0;
	conn_open = NULL;
	conn_stats_runnnig = NULL;
	seconds_to_wait_before_check_result = 1;
	start_time := Clock_timestamp();

	-- RAISE NOTICE ' execute_parallel.connstring (%)', current_setting('execute_parallel.connstring',true);

	-- Loop for ever while not all staements done
	WHILE all_stmts_done = FALSE LOOP
		stats_done = null;
		CALL start_execute_parallel(
			stmts,
			connstr,
			conn_open,
			conn_stats_runnnig,
			stats_done,
			all_stmts_done,
			seconds_to_wait_before_check_result,
			max_parallel_jobs,
			close_open_conn,
			contiune_after_stat_exception
		);

		IF loop_number = 1 AND coalesce(Array_length(conn_open, 1),0) = 0 THEN
			RAISE EXCEPTION 'In do_execute_parallel, Failed to open any connection connstr % stmts %', connstr, stmts;
		END IF;

		start_execute_parallel_counter = start_execute_parallel_counter + 1;

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		IF used_time > seconds_to_wait_before_check_result THEN
			start_time_delta_job = Clock_timestamp();
			RAISE NOTICE 'Status job length % jobs running in % threads at loop_number % at % in % secs',
			Array_length(stmts, 1), coalesce(Array_length(conn_open, 1),0), loop_number, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
		END IF;

		IF all_stmts_done = FALSE THEN
			perform pg_sleep(seconds_to_wait_before_check_result/10);
			-- COMMIT;
		END IF;

		loop_number = loop_number + 1;

	END LOOP;


END;
$$
;

GRANT EXECUTE ON PROCEDURE do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text, -- this will be buildt up inside start_execute_parallel at first call IF NULL
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
) TO public;




