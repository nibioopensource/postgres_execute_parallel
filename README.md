# What is this function doing ?
Postgres sql code to execute an array of statements in parallel based on dblink.

Returns the number of ok executed statements.

# How to use :
In the example below we send 11 statements to be executed in three parallel dblink connections .

Logged in as postgres
<pre><code>
call do_execute_parallel('{"select pg_sleep(10)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)"}'::text[],3);

NOTICE:  00000:  execute_parallel.connstring (<NULL>)
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, 11 statements to execute in 3 threads
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Init new connstr for 11 statements to execute in 3 threads using dbname=rog_02 port=5432
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(10)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_1 using index 1
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 using index 2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 using index 3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 8 to execute in 3 threads , num_stmts_running 3, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 using index 2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 using index 3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 6 to execute in 3 threads , num_stmts_running 3, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 using index 2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 using index 3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 4 to execute in 3 threads , num_stmts_running 3, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 using index 2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 using index 3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 2 to execute in 3 threads , num_stmts_running 3, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 using index 2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Started new stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 using index 3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 3, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_2 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(1)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_3 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 1, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 1, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 1, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 1, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 1, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, test if done for stmt (select pg_sleep(10)) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_1
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done with stmt (<NULL>) on connection conn_344eeee002a7bcd9af3c29d20e0b878e_1 and retv <NULL>
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel num statements left 0 to execute in 3 threads , num_stmts_running 0, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
NOTICE:  00000: In start_execute_parallel, Done execute in 3 threads, num_stmts_failed in this run 0
LOCATION:  exec_stmt_raise, pl_exec.c:3910
CALL
Time: 10321,543 ms (00:10,322)

</pre></code>

In this case the following is hapning :
- The first statements takes 10 seconds and the first dblink connection works this all the time with this statement.
- The 2 other connections continue to work with the rest of the statements while the first dblink connection work the first statement.

This mean that that this jobs takes a total of 10 seconds if there are three free database connections .

If you request to run more parallel requests than there are db connections available,
the number of parallel jobs will be reduced to the number of available connections.

If you have a error in one of the statements an exception will be thrown with the error.

It will execute the rest of the statements if not the _contiune_after_stat_exception is set false when called, the default value is true.


# How to install :

git clone https://gitlab.com/nibioopensource/postgres_execute_parallel.git

cat postgres_execute_parallel/src/main/sql/function*.sql | psql

psql -c'CREATE EXTENSION dblink;'

# Info
Based on initial code from Joe Conway <mail@joeconway.com>  in https://www.postgresql-archive.org/How-to-run-in-parallel-in-Postgres-td6114510.html


