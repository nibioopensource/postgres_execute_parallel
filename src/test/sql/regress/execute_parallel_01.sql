-- To avoid error since binary geo seems to vary from local systen and Travis

SET client_min_messages TO WARNING;

CREATE EXTENSION dblink; -- needed by  execute_parallel
-- set connect string
set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';


\i ../../../../src/main/sql/function_execute_parallel.sql

-- Test calling with out wait
CALL do_execute_parallel('{"SELECT  pg_sleep(10)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)"}'::text[],NULL,3,true);

-- Test create table
CALL do_execute_parallel('{"create table test(c1 int)"}'::text[],NULL,3);

-- Test insert rows in table (one invalid sql commamd),  with out wait
/**
CALL do_execute_parallel('{"insert  into testfeil) values (2)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],3);
SELECT  5, count(*) from test where c1=1;
SELECT  6, count(*) from test where c1=2;
*/

-- Test insert rows in table, with out wait
CALL do_execute_parallel('{"insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],NULL,10);
SELECT  8, count(*) from test where c1=1;
SELECT  9, count(*) from test where c1=2;


-- Test insert rows in table, with  wait by using start_execute_parallel
DO $$
<<test_parallel>>
declare

	-- start used by start_execute_parallel
	connstr text; -- this will be buildt up inside start_execute_parallel at first call
	conn_open text[]; -- this will be buildt up inside start_execute_parallel at first call
	conn_stats_runnnig text[]; -- this will be buildt up inside start_execute_parallel at first call
	stats_done text[]; -- This should be null when called , and new statements done will be added to this list.
	all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
	start_execute_parallel_counter int = 0;
	seconds_to_wait_before_check_result int = 300;
	-- done used by start_execute_parallel

	stmts text[];
	used_time float;
	start_time_delta_job timestamp WITH time zone;
	max_parallel_jobs int;
	contiune_after_stat_exception boolean;
	start_time timestamp WITH time zone;
	loop_number int = 1;


begin

	stmts = '{"insert  into test(c1) values (3)","insert  into test(c1) values (3)","insert  into test(c1) values (3)"}'::text[];
	max_parallel_jobs = 2;
	contiune_after_stat_exception = true;

	all_stmts_done = false;
	start_execute_parallel_counter = 0;
	connstr = NULL;
	conn_open = NULL;
	conn_stats_runnnig = NULL;
	seconds_to_wait_before_check_result = 1;
	start_time := Clock_timestamp();

	WHILE all_stmts_done = FALSE LOOP
		stats_done = null;

		CALL start_execute_parallel(
			stmts,
			connstr,
			conn_open,
			conn_stats_runnnig,
			stats_done,

			all_stmts_done,
			seconds_to_wait_before_check_result,
			max_parallel_jobs,
			true,
			contiune_after_stat_exception
		);
		start_execute_parallel_counter = start_execute_parallel_counter + 1;

		RAISE NOTICE 'stats_done in this run % (%),  jobs running now % (%)',
		coalesce(Array_length(stats_done, 1),0), stats_done,
		coalesce(Array_length(conn_stats_runnnig, 1),0), conn_stats_runnnig;

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		IF used_time > seconds_to_wait_before_check_result THEN
			start_time_delta_job = Clock_timestamp();
			RAISE NOTICE 'Status job length % jobs running in % threads at loop_number % at % in % secs',
			Array_length(stmts, 1), coalesce(Array_length(conn_open, 1),0), loop_number, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
		END IF;

		RAISE NOTICE 'Status job length % jobs running in % threads at loop_number % at % in % secs',
		Array_length(stmts, 1), coalesce(Array_length(conn_open, 1),0), loop_number, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

		IF all_stmts_done = FALSE THEN
			perform pg_sleep(seconds_to_wait_before_check_result/10);
			COMMIT;
		END IF;


	END LOOP;


end test_parallel $$ ;

SELECT  11, count(*) from test where c1=3;


drop table test;

-- so for now i just drop it here
DROP PROCEDURE IF EXISTS do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text, -- this will be buildt up inside start_execute_parallel at first call IF NULL
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
);
DROP PROCEDURE IF EXISTS start_execute_parallel(
INOUT _stmts text[],
INOUT connstr text,
INOUT conn_open text[],
INOUT conn_stats_runnnig text[],
INOUT stats_done text[],
INOUT all_stmts_done boolean,
_default_seconds_to_wait int,
_num_parallel_thread int,
_close_open_conn boolean,
_contiune_after_stat_exception boolean);
DROP extension IF EXISTS dblink cascade;

/**

CREATE EXTENSION dblink; -- needed by  execute_parallel
select 1, execute_parallel('{"select pg_sleep(10)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)"}'::text[],3,true);
select 2, execute_parallel('{"select aaaapg_sleep(10)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)"}'::text[],3,false,true);
select 3, execute_parallel('{"create table test(c1 int)"}'::text[],3);
select 4, execute_parallel('{"insert  into testfeil) values (2)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],3);
select 5, count(*) from test where c1=1;
select 6, count(*) from test where c1=2;
select 7, execute_parallel('{"insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],10);
select 8, count(*) from test where c1=1;
select 9, count(*) from test where c1=2;
select 10, execute_parallel('{"select 1/0","select pg_sleep(1)"}'::text[],3,true,null,false);
drop table test;
*/