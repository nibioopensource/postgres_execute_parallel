-- To avoid error since binary geo seems to vary from local systen and Travis

SET client_min_messages TO ERROR;

CREATE EXTENSION dblink; -- needed by  execute_parallel

\i ../../../../src/main/sql/function_execute_parallel.sql

-- Test create table
create table test(c1 int);

-- Test insert rows in table (one invalid sql commamd),  with out wait
/**
CALL do_execute_parallel('{"insert  into testfeil) values (2)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],3);
SELECT  5, count(*) from test where c1=1;
SELECT  6, count(*) from test where c1=2;
*/



-- Create a user user01_test_execute_parallel and grant access to table test
DO $$
BEGIN
CREATE ROLE user01_test_execute_parallel;
EXCEPTION WHEN duplicate_object THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
ALTER ROLE user01_test_execute_parallel WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'testp101';
GRANT ALL ON TABLE test TO user01_test_execute_parallel;

-- Set execute_parallel.connstring to a non exting user no rows should be added
set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=user01_not_exist password=testp101';
DO $$
BEGIN
CALL do_execute_parallel('{"insert into test(c1) values (15)","insert into test(c1) values (15)","insert into test(c1) values (15)","insert into test(c1) values (15)"}'::text[]);
EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SELECT  1, count(*) from test where c1=15;

SET execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=user01_test_execute_parallel password=testp101';
--SET execute_parallel.password = 'testp101';
--Not able det only password to work with CI
DO $$
BEGIN
CALL do_execute_parallel('{"insert into test(c1) values (25)","insert into test(c1) values (25)","insert into test(c1) values (25)","insert into test(c1) values (25)"}'::text[]);
EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SELECT  2, count(*) from test where c1=25;

drop table test;

-- so for now i just drop it here
DROP PROCEDURE IF EXISTS do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text, -- this will be buildt up inside start_execute_parallel at first call IF NULL
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
);
DROP PROCEDURE IF EXISTS start_execute_parallel(
INOUT _stmts text[],
INOUT connstr text,
INOUT conntions_array text[],
INOUT conn_stmts text[],
INOUT stats_done text[],
INOUT all_stmts_done boolean,
_default_seconds_to_wait int,
_num_parallel_thread int,
_close_open_conn boolean,
_contiune_after_stat_exception boolean);
DROP extension IF EXISTS dblink cascade;

/**

CREATE EXTENSION dblink; -- needed by  execute_parallel
select 1, execute_parallel('{"select pg_sleep(10)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)"}'::text[],3,true);
select 2, execute_parallel('{"select aaaapg_sleep(10)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)","select pg_sleep(1)"}'::text[],3,false,true);
select 3, execute_parallel('{"create table test(c1 int)"}'::text[],3);
select 4, execute_parallel('{"insert  into testfeil) values (2)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],3);
select 5, count(*) from test where c1=1;
select 6, count(*) from test where c1=2;
select 7, execute_parallel('{"insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],10);
select 8, count(*) from test where c1=1;
select 9, count(*) from test where c1=2;
select 10, execute_parallel('{"select 1/0","select pg_sleep(1)"}'::text[],3,true,null,false);
drop table test;
*/