
DROP PROCEDURE IF EXISTS do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text,
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
);


DROP PROCEDURE IF EXISTS start_execute_parallel(
INOUT _stmts text[],
INOUT connstr text,
INOUT conntions_array text[],
INOUT conn_stmts text[],
INOUT all_stmts_done boolean,
_default_seconds_to_wait int,
_num_parallel_thread int,
_close_open_conn boolean,
_contiune_after_stat_exception boolean);


DROP extension IF EXISTS dblink cascade;