DROP PROCEDURE IF EXISTS proc_test_error_job(_in_value int, _max_values int);

CREATE OR REPLACE PROCEDURE proc_test_error_job(_in_value int, _max_values int)
LANGUAGE plpgsql
AS $$
DECLARE
num_values int;
is_done boolean;
BEGIN

insert into ttest(c1) values (_in_value);

END;
$$;


-- Grant som all can use it
GRANT EXECUTE ON PROCEDURE  proc_test_rerun_job(_in_value int, _max_values int) to PUBLIC;


--select * from proc_test_rerun_job(1,2);
--show execute_parallel.rerun_job;