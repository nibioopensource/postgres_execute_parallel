DROP PROCEDURE IF EXISTS proc_test_rerun_job(_in_value int, _max_values int);

CREATE OR REPLACE PROCEDURE proc_test_rerun_job(_in_value int, _max_values int)
LANGUAGE plpgsql
AS $$
DECLARE
num_values int;
is_done boolean;
BEGIN

insert into test(c1) values (_in_value);

SELECT  count(*) from test INTO num_values;

IF num_values >= _max_values THEN
	is_done = true;
	set execute_parallel.rerun_job=false;
ELSE
	is_done = false;
	set execute_parallel.rerun_job=true;
END IF;


END;
$$;


-- Grant som all can use it
GRANT EXECUTE ON PROCEDURE  proc_test_rerun_job(_in_value int, _max_values int) to PUBLIC;


--select * from proc_test_rerun_job(1,2);
--show execute_parallel.rerun_job;