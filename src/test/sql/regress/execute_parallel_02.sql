-- Test with using valid user called user01_test_execute_parallel

--to test a sinle test
--make; \
--time src/test/sql/regress/run_test.pl --verbose --spatial_ref_sys  ./src/test/sql/regress/execute_parallel_02 \

--SELECT pg_read_file('pg_hba.conf');

-- To avoid error since binary geo seems to vary from local systen and Travis
SET client_min_messages TO WARNING;
CREATE EXTENSION dblink; -- needed by  execute_parallel

\i ../../../../src/main/sql/function_execute_parallel.sql
\i ./function_test_rerun_job.sql
\i ./proc_test_rerun_job.sql
\i ./proc_test_error_job.sql

-- Test create table
create table test(c1 int NOT NULL);

-- Create a user user01_test_execute_parallel and grant access to table test
DO $$
BEGIN
CREATE ROLE user01_test_execute_parallel;
EXCEPTION WHEN duplicate_object THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
ALTER ROLE user01_test_execute_parallel WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'testp101';
GRANT ALL ON TABLE test TO user01_test_execute_parallel;

-- Set execute_parallel.connstring so user user01_test_execute_parallel can connect by using dblink 3 rows should be added
set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=user01_test_execute_parallel password=testp101';


-- Test plain inserts
CALL do_execute_parallel('{"insert into test(c1) values (4)","insert into test(c1) values (4)","insert into test(c1) values (4)"}'::text[]);
SELECT  1, count(*) from test where c1=4;

-- clear out old value
delete from test;

-- Test inserts by calling a function sets execute_parallel.rerun_job to true when numbers of rows < 13
CALL do_execute_parallel('{"select function_test_rerun_job(100,13)"}'::text[]);
SELECT  'num after calling function_test_rerun_job (13) ', count(*) from test;

-- Test inserts by calling a procedure sets execute_parallel.rerun_job to true when numbers of rows < 30
CALL do_execute_parallel('{"CALL proc_test_rerun_job(100,33)"}'::text[]);
SELECT  'num after calling proc_test_rerun_job (33) ', count(*) from test;

SET client_min_messages TO ERROR;
DO $$
BEGIN
	-- Test inserts table dies not exits
	-- Test error handling, when invalid value
	CALL do_execute_parallel('{"CALL proc_not_found(NULL,NULL)","CALL proc_test_rerun_job(NULL,NULL)", "CALL proc_test_rerun_job(100,43)","CALL proc_test_error_job(100,43)"}'::text[]);
	EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SET client_min_messages TO WARNING;

SELECT  'num after calling proc_test_rerun_job (43) ', count(*) from test;


drop table test;

-- Test error handling, when table does not exit
SET client_min_messages TO ERROR;
DO $$
BEGIN
	-- Test inserts table dies not exits
	CALL do_execute_parallel('{"CALL proc_test_rerun_job(100,30)"}'::text[]);
	SELECT  'num after calling proc_test_rerun_job', count(*) from test;
	EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SET client_min_messages TO WARNING;

-- clean up
DROP FUNCTION IF EXISTS function_test_rerun_job(_in_value int, _max_values int);
DROP PROCEDURE IF EXISTS proc_test_rerun_job(_in_value int, _max_values int);
DROP PROCEDURE IF EXISTS proc_test_error_job(_in_value int, _max_values int);


-- Not able to get ./run_test.pl --after-test-script=execute_parallel-cleanup.sql to work from got-lab .gitlab-ci.yml
-- Tries to find file at /gcl-builds/src/test/sql a
-- so for now i just drop it here
DROP PROCEDURE IF EXISTS do_execute_parallel(
	_stmts text[], -- The list of statements to run
	_connstr text, -- this will be buildt up inside start_execute_parallel at first call IF NULL
	_num_parallel_thread int, -- number threads/connections to use
	_close_open_conn boolean, -- always close connection before and open before sending next statement
	_contiune_after_stat_exception boolean -- If true, will contiunue to run even if one the statements fails
);
DROP PROCEDURE IF EXISTS start_execute_parallel(
INOUT _stmts text[],
INOUT connstr text,
INOUT conntions_array text[],
INOUT conn_stmts text[],
INOUT stats_done text[],
INOUT all_stmts_done boolean,
_default_seconds_to_wait int,
_num_parallel_thread int,
_close_open_conn boolean,
_contiune_after_stat_exception boolean);
DROP extension IF EXISTS dblink cascade;
